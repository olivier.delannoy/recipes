# Granary Bread 

- Cooking time 30 mins
- Resting time 2hrs + 30 mins 

## Ingredients

- For the dough
    - 500 g Granary or Malthouse flour
    - 300 ml (9 fl oz) lukewarm water
    - 2 tsp salt
    - 2 tsp dried yeast
- For the glaze
    - ½ tsp salt in 1 tbsp water
- For the topping
    - Rolled oats or wheat flakes

## Instructions 
- Add the yeast to the water, mix well and leave for 10 minutes to activate.
- Combine the flour, salt and yeast water in a large bowl and mix well to form a slightly tacky dough. You may need to leave the dough rest for a few minutes to allow the water to equilibrate.
- Turn the dough out onto a lightly floured surface and knead well for about 10 minutes.
- Place the dough in a lightly-oiled bowl, cover and put in a warm place to allow the dough to double in size.
- Turn the risen dough out onto a lightly floured surface, knock it back then knead for 2-3 minutes.
- Form the dough into a smooth ball and place on a well-floured baking tray. Cover with a large bowl and leave in a warm place for 30-40 minutes to allow the dough to prove.
- Brush the dough with the glaze and sprinkle on the topping. Bake at 210°C/410°F fan oven, 230°C/450°F conventional oven for 10 minutes.
- Turn the oven down to 180°C/360°F fan oven, 200°C/390°F conventional oven and continue baking for 15-20 minutes.
- Place the cooked cob on a wire rack to cool.


## Alternative receipe 
- [Six steps to a perfect granary loaf](https://www.dailymail.co.uk/femail/food/article-1266395/Recipe-Six-steps-perfect-granary-loaf.html)
