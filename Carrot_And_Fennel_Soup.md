# Carrot and fennel soup

Serves: 4

## Ingredients

* 1Kg carrots
* 1 onion 
* 2 bulbs of fennel 
* 2 cloves of garlic 
* 1 teaspoon of cumin seeds 
* 1 teaspoon of fennel seeds 
* 1.6 litres of organic vegetable stock 

## Recipe

Preheat the oven to 180c 

Peel the carrots and onion, then trim the fennel bulbs. 

Cut these vegetables into chunks and place in a roasting tray, drizzle with 2 tablespoons of oil and sprinkle over the cumin and fennel seeds, 
season and roast for 20 minutes. 

Add the peeled garlic cloves, stir all the vegetables and roast for a further 10 to 15 minutes unitl everything is soft and nicely roasted.

Put the roasted veg in a large pan with the vegetable stock and bring to the boil.

Simmer gently for 15 minutes, then blend until smooth, taste and season with salt and pepeer if needed. 
