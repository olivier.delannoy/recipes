# Parsnip and Rosemary Bread

## Ingredients
* Olive Oil
* 220 g of corse grated parsnip
* 275 g of self raising flour
* 1 table spoon of finely chopped rosemary
* 1 tea spoon of salt
* 2 beaten eggs
* 2 table spoon of milk

## Instructions
* Preheat oven at 190 degree (th. 6-7)
* In a large bowl, mix together the parsnips, flour, rosemary and salt. 
  Create a well in the middle and add the eggs and the milk in the middle.
  With a knife mix the ingredients quickly to obtain a rough dought. Avoid 
  overworking the dough.
* Split the dough in 6 and shapes individual breads cut the top and stick a 
  twig of rosemary in each of the bread 
* Put the bread on a baking tray oiled or covered with a non sticky sheet
* Cook the bread for 25 to 30 minutes until golden. 
* You can let them cool down or eat them warm, they are even better that way.  