# Praline Chocolate Log (Buche au chocolat praline)
Preparation time: 45 minutes
Cooking oven: 15 mn - th 6/180 C

## Ingredients
### Biscuit
- 5 eggs
- 150g sugar
- 120g flour
- 75g butter

### Ganache
- 30cl Single Cream or Wipping Cream (To try)
- 150g of milk chocolate (good quality)
- 100g Praline paste (I use a mixture of hazelnut powder + almond powder if I don't find praline powder or paste)


### Caramel
- 50g sugar


### Icing
- 100g flaked hazelnut
- 100g sugar
- 10cl creme fraiche
- 20g butter
- 25g milk chocolate

## Recipe
### Caramel
Prepare a caramel with sugar and three table spoons of water.
When it starts to colour take off the fire and throw half a glass of water.
Dissolve the caramel by putting the sauce pan back on the fire while mixing for a few moments.
Let it cool down.

### Prepare the ganache
Simmer the cream.
Out of the fire add the chocolate broken in pieces and the praline paste.
Let the chocolate melt without mixing for three minutes.
Mix with a spatula gently until the mix is homogeneous.
Let to rest in the fridge for at least an hour.

### Prepare the biscuit
Preheat the oven at 180C/Gas mark 6
Whisk the egg yolk with the sugar in a bowl on top of a sauce pan with boiling water for four minutes.
Once the mix start to get thicker, Add the flour and melted butter.
Whisk egg whites until firm and mix in with the rest of the ingredient gently.
Spread the preparation with a spatula on a rectangular baking tray (I use backing parchment to ease taking off the biscuit).
Cook for 10 to 15 minutes until the top of the biscuit is of a pale yellow colour.
When taking the biscuit out of the oven flip over on a wet cloth before taking the baking parchment out.

### Put the cake together
Take the ganache out of the fridge and mix for five minute using a whisk until it rise and double in volume.
With a brush cover the biscuit with the liquid caramel.
Wrap the biscuit in a cake tin letting about 12 cm on one side (to close).

> Careful it is the difficult part of it all as the biscuit tends to break.

Fill the content of the cake tin with the ganache.
Cover the ganache with the side of the biscuit to close the log.
Cover the cake with foil so that the foil is quite tight on top of the biscuit.
Put a glass bottle (wine) or two tins on top of the cake and put in the fridge for
at least three hours.
Take the log out of the cake tin gently and with your hand round the top if needed.

### Prepare the icing
Put the hazelnut in a sauce pan and grill them, once slightly colour put aside out of the sauce pan (to avoid further cooking... and burning :)).
Prepare a caramel with the sugar and three table spoons of water.
When it starts to colour take off the fire and add the creme fraiche.
With a spatula dissolve the caramel.
Add the butter and let it simmer for 2 minutes.
Take of the fire and add the chocolate and the roasted hazelnut flakes.
Mix throughout and let it cool down a bit.
When the caramel start to become thicker, cover the log with the icing and level the icing with a spatula
Let it rest and keep it a cold place until time of service.




