# Paul's Fougasse 

For 3 or 4 Fougasse 

## Ingredients 

* 500g flour
* 10g salt,
* 10g instant yeast
* 2 Tbsp olive oil
* 380ml water possibly less
* 2 tsp Italian herb seasoning or whatever herbs you've got. I like to chop up some green or black olives and add those too.


## Steps 

* Mix and knead for 10 mins.
* Leave to rise for an hour.
* Turn out, divide into 3 or 4
* Dust worktop with flour or flour and semolina then put one portion on a baking tray and dust with flour/semolina mix, then using your fingers spread into a flat oval.
* Use a knife or pizza cutter cut two long holes down the length of the oval and 3 slashes on either side of that to give you a leaf shape. 
* Stretch out the dough to accentuate the holes/shape, put in a plastic bag and leave to prove for about half an hour.
* Get oven pre-heated to 200°C (fan).
* Spray with oil and sprinkle the fpugasse with sea salt and bake for 15 - 20 minutes. 


**Enjoy** 
