# My Collection of recipes in english


## Side dishes
- [Gratin Dauphinois](/Gratin_Dauphinois.md)
## Main dishes 
- [Carrot and fennel soup](/Carrot_And_Fennel_Soup.md)
## Breads and Brioches
- [Granary Bread](/Granary_Bread.md)
- [Parsnip and rosemary Bread](/Parsnip_and_rosemary_bread.md)
- [Brioche](/Brioche.md)
- [Fougasse](/Paul_Fougasse.md)

## Pastries
- [Croissants](/Croissants.md)


## Deserts
- [Praline Log](/Praline_Log.md)
