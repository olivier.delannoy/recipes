# Gratin Dauphinois / Dauphinois potatoes
- Serves four people

## Ingredients
- 1 clove garlic
- Some butter for the dish
- 1 Kg Yukon Gold / Charlotte potatoes peeled
- 3 eggs
- 500ml Whipping cream
- Salt and Pepper

## Preparation
- Preheat oven at 150 C
- Rub an 8 inch (20 cm cast-iron skillet or ovenproof dish  well with the garlic and butter.
- Slice the potatoes very thinly with a mandolin. Rinse under cold water and pat to dry.
- In a large bowl, lightly beat together the eggs, cream, salt and pepper to taste.
- Toss the potatoes in the mixture to coat.
- Transfer the potatoes and liquid to the pan.
- Bake for about 1 hour until a light golden brown.
