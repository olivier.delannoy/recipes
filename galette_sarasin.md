# Galette De Sarasin

## Ingredients: 
- 250 g farine de sarasin
- 2 cuilleres a cafe de farine de ble
- 1 pincee de sel
- 1 pincee de poivre 
- 1 oeuf
- 2 cuilleres a cafe d'huile

## Preparations
- Melangez la farine de sarasin avec le jaune d'oeuf et ajoutez l'eau
- Montez le blanc d'oeuf en neige avec le sel et le poivre. Y ajoutez la farine de ble a petite vitesse, puis l'huile
- Melangez les deux preparations et ajoutez un peu d'eau si necessaire 
- Laissez reposer 1 heure. 