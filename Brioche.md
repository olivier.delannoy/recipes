# Brioche 

This recipe is for 600g of Dough. 

- Preparation time: 35 minutes
- Resting time 2h
- Raising time for the dough 3h30
- Cooking time 10 to 12 minutes 

## Ingredients
### For the dough
- 250g of flour
- 30 g sugar
- 1 teaspoon of salt
- 10 g of fresh yeast
- 3 eggs (150g) 
- 165 of butter (soft at room temperature) 
### Finish
- 2 egg yolks

## Prepare the dough
1. Put the flour, sugar, salt, yeast in the mixer bowl (yeast should not enter 
   in contact with the with the salt or sugar). 
1. Add eggs, and knead for 2 to 3 minutes at slow speed
1. Add the butter and increase to medium speed for 5 to 10 minutes until dough 
   is elastic. The dough should detach from the side of bowl. And the dough is 
   ready when you can grab it with your hand. 
1. Cover the bowl with a towel and leave it to raise for at least an hour at 
   room temperature until it double its volume. 
1. Shape it in a roll and let it harden in the fridge for at least 2 hours. 
1. Shape to the desired form. I often do plaits splitting the dough in 3 rolls. 
1. Once shaped let the dough rest at room temperature for another 2 hours.
1. Apply egg yolks using a brush on the dough and cook for 10 to 12 minutes in 
   preheated oven. 