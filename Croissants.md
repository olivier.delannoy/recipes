# Croissants facon boulangerie

## Ingredients
(For about 18 croissants)

- 500 g of flour (T45)
- 20 g of fresh yeast or 10 g of dried yeast
- 60 g of sugar
- 10 g of salt
- 200ml of milk
- 2 eggs + 1 to brown
- 200 g unsalted butter

## Recipe

### Prepare the dough

- Put the flour in a large bowl and make a well and crumble the yeast
- Add sugar and salt making sure that salt and sugar do not touch the yeast
- Add the warm milk and eggs
- Knead the dough and make a ball
- Rest the dough for around an hour in a warm room.
- Once the dough has doubled in size, knead the dough for a few moments to make it deflate and put in the
  fridge for half an hour. (higher in the fridge works best)

### Spread the dough

- Spread the dough on a work top well floured using a roller. The butter should be about as hard as the dough.
- Put the butter in the middle of the dough and spread on the dough without putting butter on the side of the dough.
- Wrap the butter with the dough, make sure the butter is fully covered with the dough and that the side of the dough
  are secured pressing them using wet finger.

### Puffing the dough
The below need to be done twice (at least) letting the dough rest in the fridge at least 30 minutes in between in order to achieve 16 folds.
Finally at the end I tend to let the dough in the fridge for at least an hour to ensure no butter escapes while spreading it.

- Spread the dough again making sure that the butter does not escape in a rectangular shape long enough to do four fold
- Fold the outer side toward the middle and repeat to obtain four layer.
- Turn the dough by a quarter (always turn in the same direction)
- Repeat the above.

Note: if the dough is too soft, make sure you put it in the fridge instead of trying to do a fold.

### Shape the croissants
- Spread the dough thinly to achieve a 40x50cm rectangle.
- Cut in half to achieve a 20x50cm rectangle.
- Cut triangle of about 20x10cm
- Roll triangle finishing by the tip.
- Put the croissants on a tray with baking paper with spaces between them and let them rest for 1 hour
- Apply the egg yolk diluted with a bit of water on the croissant
- Cook for 10/15 minutes at 210 C checking for colour

Note:
 - Don't get them burnt as it goes quickly from cooked to over cooked.
 - I tend to have a nice and hot oven, with a tray with water at the bottom
 - I find that it work quite well to get the dough done in the evening and rest overnight in the
   fridge. I do the shaping and cooking done in the morning.










